; ce programme est pour l'assembleur RealView (Keil)
	thumb
	area  moncode, code, readonly
	export timer_callback
	extern etat
;


GPIOB_BSRR	equ	0x40010C10	; Bit Set/Reset register
	

timer_callback proc
	
; mise a 1 de PB1
	ldr r1, =etat
	ldr r1, [r1]
	ldr	r3, =GPIOB_BSRR
	cmp r1, #0x00020000
	beq eq
	bne noteq
	
gdc	ldr r0, =etat
	str r1, [r0]
	str	r1, [r3]
; N.B. le registre BSRR est write-only, on ne peut pas le relire

	bx	lr
	endp
		
eq	
	mov r1, #0x00000002
	b	gdc
	
noteq
	mov	r1, #0x00020000
	b	gdc
	
	
	
	end
	